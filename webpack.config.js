const settings = require('./settings');


sts = new settings();

module.exports = env => {

    const webpack = {};

    webpack.mode = env;
    webpack.watch = true;
    webpack.devtool = 'source-map';

    const entry = webpack.entry = {};
    const output = webpack.output = {};
    const module = webpack.module = {};
    const rules = module.rules = [];
    const plugins = webpack.plugins = [];

    entry.app = sts.entry;

    output.filename = sts.filename;
    output.path = sts.path;
    output.sourceMapFilename = sts.sourceMapFilename;
    output.chunkFilename = sts.chunkFilename;

    [
        sts.js
    ].forEach(item => rules.push(item));

    [
        sts.html_plugin
    ].forEach(item => plugins.push(item));

    // console.log(webpack.plugins);

    webpack.optimization = {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    };

    return webpack;
}

module.exports();