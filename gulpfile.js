const { src, dest, series, parallel, watch } = require('gulp');
const sass = require('gulp-dart-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

const path = {
    sass: 'src/**/*.scss',
    javascript: 'src/**/*.js'
}

function sassTask() {
    return src([path.sass])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write('./sourcemaps/css'))
        .pipe(dest('public'))
}


function watchTask() {
    browserSync.init({
        open: 'external',
        proxy: 'localhost',
        port: 8080
    });
    watch([path.sass], parallel(sassTask)).on('change', browserSync.reload);
    watch([path.javascript]).on('change', browserSync.reload);
}

exports.default = series(
    parallel(sassTask),
    watchTask
);