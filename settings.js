const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

class Settings {
    constructor() {
        this.entry = './src/index.js';
        this.filename = '[name].bundle.js';
        this.path = path.resolve(__dirname, 'public');
        this.sourceMapFilename = 'sourcemaps/javascript/[file].map';
        this.chunkFilename = '[id].js';
        this.js = {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        };
        this.html_plugin = new HtmlWebpackPlugin({
            filename: 'home.php',
            template: path.resolve(__dirname, './src/index.php')
        });
    }
}

module.exports = Settings;